# This is customize the module installation process if you need
SKIPUNZIP=0

# Module Introduction
ui_print "模块基本信息"
ui_print "版本号：patch1for2.0only"
ui_print "制作开始日期：2020.07.28"

ui_print "模块作者：Coolapk(@Sun思晴);Github(@Sunshine018)"
ui_print ""

# Caution
ui_print ""
ui_print "注意！"
ui_print "安装完成之后一定要在Recovery下清除Cache分区！"
ui_print "否则会出现Google服务FC等非正常情况！"
ui_print ""

# License
ui_print "License:GNU General Public License 3.0"